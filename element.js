const template = document.createElement('template');
template.innerHTML = `<style>
.user-card{
    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
    background: #f4f4f4;
    width:500px;
    display:grid;
    grid-gap:10px;
    margin-bottom:15px;
    border-bottom:5px solid blue;
}

.user-card img{
    width:100%;
}

.user-card button{
    cursor: pointer;
    background: #f4f4f4;
    color:blue;
    border:0;
    border-radius: 5px;
    

}
</style>
<div class="user-card">
<h2 class="name">ram</h2>
<img />
<div class="info">
<p><slot name="name"></p>
<p><slot name="email"></p>
</div>
<button id="toggle-info">Hide Info</button>
</div>
`;

class MyProduct extends HTMLElement {
    constructor() {
        super();
        this.showInfo = true;
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        const username=this.shadowRoot.querySelector('.name');
        username.innerText = this.getAttribute('user-name');
        this.shadowRoot.querySelector('img').src = this.getAttribute('avatar');
    }

    toggleInfo() {
        this.showInfo = !this.showInfo;
        const info = this.shadowRoot.querySelector('.info');
        const toggleBtn = this.shadowRoot.querySelector('#toggle-info');
        if (this.showInfo) {
            info.style.display = 'block';
            toggleBtn.innerText = 'hide info';
        } else {
            info.style.display = 'none';
            toggleBtn.innerText = 'show info';
        }
    }
    connectedCallback() {
        this.shadowRoot.querySelector('#toggle-info').addEventListener('click', () => this.toggleInfo());
    }
    disconnectedCallback() {
        this.shadowRoot.querySelector('#toggle-info').removeEventListener();
    }

}
function addCustomElement() {
    customElements.define('my-product', MyProduct);
    console.log("Added MyElement to custom element registry!");
}
addCustomElement();

