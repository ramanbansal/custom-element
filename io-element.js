const template = document.createElement('template');
template.innerHTML = `<style>
:host .user-card{
    font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
    background: #f4f4f4;
    width:500px;
    display:grid;
    grid-gap:10px;
    margin-bottom:15px;
    border-bottom:5px solid blue;
}

:host .user-card img{
    width:100%;
}

:host .user-card button{
    cursor: pointer;
    background: #f4f4f4;
    color:blue;
    border:0;
    border-radius: 5px;
    

}
</style>
<div class="user-card">
<div class="info">
<p><slot name="name"></p>
<p><slot name="email"></p>
</div>
<button id="toggle-info">Hide Info</button>
</div>
`;



class MyProduct extends HTMLElement {
    static get observedAttributes() {
        return ['open'];
    }
    get open() {
        return this.hasAttribute('open');
    }

    set open(val) {
        if (val) {
            this.setAttribute('open', '');
        } else {
            this.removeAttribute('open');
        }
    }
    toggleInfo() {
        // this.showInfo = !this.showInfo;
        const info = this.shadowRoot.querySelector('.info');
        const toggleBtn = this.shadowRoot.querySelector('#toggle-info');
        if (this.open) {
            info.style.display = 'block';
            toggleBtn.innerText = 'hide info';
        } else {
            info.style.display = 'none';
            toggleBtn.innerText = 'show info';
        }
    }

    // Only called for the disabled and open attributes due to observedAttributes
    attributeChangedCallback(name, oldValue, newValue) {
        this.toggleInfo();

        // TODO: also react to the open attribute changing.
    }

    constructor() {
        // If you define a constructor, always call super() first!
        // This is specific to CE and required by the spec.
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        // it used for body click
        // this.addEventListener('click', e => {}
        // Setup a click listener on <app-drawer> itself.
        // it uses for button click
        this.shadowRoot.querySelector('#toggle-info').addEventListener('click', e => {
            // Don't toggle the drawer if it's disabled.
            this.open = !this.open;
            this.dispatchEvent(new CustomEvent('change', {
                detail: {
                    open: this.open,
                },
                bubbles: true,
            }));
        });
    }

    connectedCallback() {
        this.shadowRoot.querySelector('#toggle-info').addEventListener('click', () => this.toggleInfo());
    }
    disconnectedCallback() {
        this.shadowRoot.querySelector('#toggle-info').removeEventListener();
    }

    // toggleDrawer() {
    //     this.disabled = !this.disabled;
    // }

}
function addCustomElement() {
    customElements.define('my-product', MyProduct);
    console.log("Added MyElement to custom element registry!");
}
addCustomElement();

